export const constraints = {
    email: {
      presence: {
        allowEmpty: false,
        message: "field cannot be blank."
      },
      email: {
        message: "address was not valid."
      }
    }
  };
  
  export default constraints;
