exports.events = [
    {
        key: 0,
        id: 'reindeer',
        title: "Caribou Rendezvous",
        quote: "Genius",
        plot: "It's only two days 'til Christmas and Santa's elves have made an awful discovery. Someone has been taking bribes and sneaking naughty kids to the nice list! Could it be Dasher, the popular one? Dancer, the sensitive soul? Olive, the 'other' reindeer? They and their caribou classmates are your suspects, and you've only got 3 hours to figure out whodunit.",
        summary: "A brother & sister duo raced against the clock, using the skills their parents summarized to us; transposing music, country flags, memory games, and Harry Potter trivia. They caught their culprit just in time, finding their Christmas presents in the process."
    },
    {
        key: 1,
        id: 'halloween',
        title: "Stone Family Mystery",
        quote: "This was AWESOME",
        plot: "A shriek is heard from a Federal Way mansion. The Stone family jewels have been stolen. Nine people have the means, motive and opportunity. Earn clues around town to learn who's responsible, before they get away.",
        summary: "Halloween weekend was by far Lovely Mysteries' biggest undertaking. Sixty participants raced through eight Federal Way locations, weeding out suspects at every stop. Adult trick-or-treating was a huge success."
    },
    {
        key: 2,
        id: 'styles',
        title: "Harry Styles Has Been Kidnapped",
        quote: "The most thoughtful gift I've ever received... My brain HURTS but wow the satisfaction",
        plot: "International icon Harry Styles has been kidnapped! But the evil crooks have made two mistakes - they've left behind a cryptic message, and they've underestimated a certain birthday girl's puzzle-solving abilities.",
        summary: "Olympia was such a fun setting, with lots of local businesses helping to free Mr. Styles. The puzzler's friends told us not to go easy on her when it came to word puzzles, so we shouldn't have been surprised when the birthday girl solved her first puzzle WAY faster than we expected."
    },
    {
        key: 3,
        id: 'desmoines',
        title: "Written In the Stars",
        quote: "This was so much f***ing fun",
        plot: "'I couldn't help but wonder... Is it possible to change your destiny? Turn that Five of Wands into a Three of Cups? Or is your palm's fate line permanent?' If the birthday girl solves enough puzzles around town, she just might shift the universe in her favor.",
        summary: "Other puzzlers have gone up against reindeers and thieves, but this super sleuth was battling against the UNIVERSE. The puzzler's fiancé told us she wanted her palm read, so we partnered with a local psychic as one of her stops. She successfully shifted her destiny while collecting gifts around town."
    }
];  