import React, { useState, useEffect } from 'react';
import './FormClickthrough.css';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';

function FormClickthrough(props) {

    const { onSubmitForm } = props;
    const [currentStep, setCurrentStep] = useState(0);
    const [hasBeen, setHasBeen] = useState(0);
    const [nextButton, setNextButton] = useState('hidden');
    const [backButton, setBackButton] = useState('hidden');
    useEffect(() => {
        window.setTimeout(pauseVid, 1400);
        return () => {
            const iframes = document.getElementsByTagName("iframe");
            if (iframes[0]) {
                iframes[0].parentNode.removeChild(iframes[0]);
            }
        }
    }, []);

    let oneMonthAway = new Date();
    oneMonthAway.setMonth(oneMonthAway.getMonth() + 1);

    const [formData, setFormData] = useState({
        'name': '',
        'email': '',
        'city': '',
        'usState': 'Washington',
        'date': oneMonthAway,
        'numberInParty': 'one',
        'interests': ''
    });

    const estCosts = {
        'one': '45',
        'two-four': '45-100',
        'five-twenty': '100-1,000',
        'over-twenty': '1,000+'
    }

    const playVid = () => {
        const iframes = document.getElementsByTagName("iframe")
        if (iframes[0]) { 
            const iframe = iframes[0].contentWindow;
            // eslint-disable-next-line
            iframe.postMessage('{"event":"command","func":"' + 'playVideo' + '","args":""}', '*');
            window.setTimeout(pauseVid, 1400);
            return;
        }
    }
    const pauseVid = () => {
        const iframes = document.getElementsByTagName("iframe")
        if (iframes[0]){
            const iframe = iframes[0].contentWindow;
            // eslint-disable-next-line
            iframe.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
        }
    }

    const changeStep = (pos) => {
        if (pos === 1 && nextButton === 'disabled' && hasBeen > 0) {
            return;
        }
        const newStep = currentStep + pos;
        setCurrentStep(newStep);
        if (pos === 1 && newStep > hasBeen) {
            playVid();
            setHasBeen(newStep);
        }
        prevNextButtonUpdates(newStep, pos);
    }

    const prevNextButtonUpdates = (currentStep, pos) => {
        if (currentStep >= 0 && currentStep <= 4) {
            if (currentStep >= 3 || currentStep === 0) {
                setNextButton('hidden');
            } else {
                const newStatus = (pos === 1 && currentStep > hasBeen) ? 'disabled' : 'ready';
                setNextButton(newStatus);
            }
        }
        if (currentStep >= 1 && currentStep <= 4) {
            setBackButton('ready');
        } else {
            setBackButton('hidden');
        }
    }

    const updateForm = (e) => {
        let oldState = { ...formData };
        oldState[e.target.name] = e.target.value;
        setFormData(oldState);
        if (currentStep === 1 && e.target.name === 'city'){
            return e.target.value.length ? setNextButton('ready') : setNextButton('disabled');
        }
        if (currentStep === 2 && e.target.name === 'interests') {
            return e.target.value.length ? setNextButton('ready') : setNextButton('disabled');        
        }
    }

    const updateDate = (value) => {
        let oldState = { ...formData };
        oldState.date = value;
        setFormData(oldState);
    }

    const formSubmitted = (formData) => {
        onSubmitForm(formData);
        changeStep(1);
        return false;
    }

    return (
        <>
            <div className={`scroll-button left-scroll ${backButton}`} onClick={() => changeStep(-1)}></div>
            <div className='form-content-container'>
                <div className='content-portion'>
                    <form id="im-interested-form">
                        <div className={`intake-page prelim ${currentStep === 0 ? 'show' : ''}`}>
                            <div className='prelim-copy'>This is only preliminary; no credit card required. Once we know our team can set up in your city on your preferred date, we'll send you a detailed questionnaire about what types of puzzles you want and have a video call about how hard to make the clues.</div>
                            <input className="submit-btn" type="button" onClick={() => changeStep(1)} value="START"></input>
                        </div>
                        <div className={`intake-page ${currentStep === 1 ? 'show' : ''}`}>
                            <label>DATE OF HUNT</label>
                            <Calendar
                                className={"reserve-hunt"}
                                onChange={updateDate}
                                value={formData.date}
                                calendarType={"US"}
                                minDate={oneMonthAway}
                            />
                            <input className="hidden" value={formData.date.toString()} name="date" placeholder={formData.date.toString()} readOnly />
                            <div className='city-state-row'>
                                <label>CITY</label>
                                <input type="text" name="city" className="input city" onChange={updateForm} value={formData.city}></input>
                            </div>
                        </div>
                        <div className={`intake-page ${currentStep === 2 ? 'show' : ''}`}>
                        <div className="flex-row interests">
                            <div className='interests-input'>
                                <label>INTERESTS</label>
                                <textarea className="input area" name="interests" onChange={updateForm}></textarea>
                            </div>
                            <div className="example-interests-div">
                                <label>EXAMPLE</label>
                                <p>"It's our daughter's 25th birthday. We're surprising her with a puppy! She knows a little about Doctor Who, French, and operas. Knows a LOT about BTS, interior design, Marvel, TV shows Lost and ER, flowers and embroidery."</p>
                            </div>
                        </div>
                        <div className="flex-row partynumbers">
                        <div>
                            <label># IN PARTY</label>
                            <select className="party-num-select" name="numberInParty" onChange={updateForm} value={formData.numberInParty}>
                                <option value="one">1</option>
                                <option value="two-four">2-4</option>
                                <option value="five-twenty">5-20</option>
                                <option value="over-twenty">More than 20</option>
                            </select>
                            <span>Estimated cost: ${estCosts[formData.numberInParty]}</span>
                        </div>
                        </div>
                    </div>
                    <div className={`intake-page ${currentStep === 3 ? 'show' : ''}`}>
                        <div className="flex-row inputs">
                            <div>
                                <label>NAME</label>
                                <input type="text" name="name" className="input name" onChange={updateForm} value={formData.name}></input>
                            </div>
                            <div>
                                <label>EMAIL</label>
                                <input type="email" name="email" className="input email" onChange={updateForm} value={formData.email}></input>
                            </div>
                        </div>
                        <input className="submit-btn" type="button" onClick={() => formSubmitted(formData)} value="SUBMIT"></input>
                    </div>
                    <div className={`intake-page ${currentStep === 4 ? 'show' : ''}`}>
                        {props.getErrorMessages()}
                        {props.successMessage()}
                    </div>
                    </form>
                </div>
                <div className='flower-portion' disabled>
                    {window.innerWidth >= 800 && (<>
                    <div className="title-cover top"></div>
                    <iframe className="dahlia-iframe" id="dahlia-vid" src="https://www.youtube.com/embed/n7YeVN3G_oE?controls=0&amp;start=88&amp;showinfo=0&amp;enablejsapi=1&amp;autoplay=1&amp;mute=1" frameBorder="0" title="dahlia animation" allow="accelerometer; clipboard-write; encrypted-media; gyroscope;">
                    </iframe>
                    </>)}
                </div>
            </div>
            <div className={`scroll-button right-scroll ${nextButton}`} onClick={() => changeStep(1)}></div>
        </>
    )
}

export default FormClickthrough;