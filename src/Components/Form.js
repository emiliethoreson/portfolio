import './Form.css';
import React, { useState, useEffect } from 'react';
import { validate } from 'validate.js';
import constraints from './constraints';
import FormClickthrough from './FormClickthrough';

function Form(props) {

    const [setEmailError] = useState(false);
    const [success, setSuccess] = useState(false);
    const [errors, setErrors] = useState();
    
    useEffect(() => {
        const script = document.createElement('script');
        script.src = "https://cdn.jsdelivr.net/npm/emailjs-com@2/dist/email.min.js";
        script.type="text/javascript"
        script.async = true;
        document.body.appendChild(script);
        return () => {
          document.body.removeChild(script);
        }
      }, []);


    const onSubmitForm = (formData) => {
        const validationResult = validate(formData, constraints);
        setErrors({ errors: validationResult });
        if (!validationResult) {
            props.emailjs.sendForm('contact_service', 'template_la7yclp', document.getElementById('im-interested-form'))
            .then(response => {
                if (response.status === 200) {
                    setSuccess(true);
                } else {
                    setEmailError(true);
                }});
        }
    }

    const getErrorMessages = (separator=" ") => {
        if (!errors) return [];
        const buriedErrors = errors.errors;
        for (const inputType in buriedErrors) {
            return buriedErrors[inputType].join(separator);
        }
    }

    const successMessage = () => {
        let message = success ? (<div className="success-message">We have successfully received your inquiry. We will follow up with you in the next 24 hours.</div>) : (<></>);
        return message;
    }

    return (
        <div className="form-container">
            <h2>Book Now</h2>
            <FormClickthrough 
                onSubmitForm={onSubmitForm}
                getErrorMessages={getErrorMessages}
                successMessage={successMessage}
            />
        </div>
    );
}

export default Form;