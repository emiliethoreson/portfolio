import './AboutUs.css';
import React, { useState } from 'react';
import emiliepic from "./pics/emilie_covatar.png";
import suepic from "./pics/sue_covatar.png";


function AboutUs() {
    const [emilieHover, setEmilieHover] = useState(false);
    const [sueHover, setSueHover] = useState(false);
    const [width] = useState(window.innerWidth);
    const isMobile = width < 800;
    return (
        <>
        <h2 className="about-us-title">Meet the Team</h2>
        <div className="employees-container">
            <div className="employee-pod"
            onMouseEnter={() => setEmilieHover(true)}
            onMouseLeave={() => setEmilieHover(false)}
            >            
                <img src={emiliepic} alt="avatar of emilie" className="employee-pic" id='emilie' />
                {(emilieHover || isMobile) && 
                <div className={isMobile ? "employee-bio-mobile" : "employee-bio"}>
                    <p>
                    Emilie is a software developer. She spends her free time watching Jeopardy!, drinking syrahs, and spending time with her cat, Love.
                    </p>
                </div>}
            </div>
            <div className="employee-pod"
            onMouseEnter={() => setSueHover(true)}
            onMouseLeave={() => setSueHover(false)}
            >
                <img src={suepic} alt="avatar of sue" className="employee-pic" id='sue' />
                {(sueHover || isMobile) && 
                <div className={isMobile ? "employee-bio-mobile" : "employee-bio"}>
                    <p>
                    Sue is a substitute teacher. She spends her free time solving logic puzzles, cooking new recipies, and caring for many, many children.
                    </p>
                </div>}
            </div>
        </div>
        </>
    )
}

export default AboutUs;