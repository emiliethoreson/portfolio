import './Faq.css';
import React from 'react';

function Faq() {
    return (
        <div className="faq-container">
            <h2>Frequently Asked Questions</h2>
            <div className='faq-wrapper'>
                <h3>Where do the scavenger hunts take place?</h3>
                <p>We come to you! Whether you want it all over town, in your home, or entirely online, we'll work with you to make it happen. We're also making connections with local museums, historical societies and parks.</p>
            </div>
            <div className='faq-wrapper'>
                <h3>Who are the people behind Lovely Mysteries?</h3>
                <p>That's one puzzle you'll never solve.</p>
                <p>Totally kidding, we're Sue and Emilie. We've been making puzzles for friends and family our whole lives, and, as of a few years ago, started getting good at it. Family started requesting puzzles for their friends, who referred their friends, who told us to build a website.
                </p>
            </div>
            <div className='faq-wrapper'>
                <h3>How do you calculate the costs?</h3>
                <p>All costs go toward printing, supplies, prizes, and this domain. Make sure to mention if your business is a registered nonprofit or if your party can only pay so much.</p>
            </div>
        </div>
    )
}

export default Faq;