import './Home.css';
import React from 'react';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import federalway from './pics/FEDERALWAY.png';
import denver from './pics/DENVER.png';
import silverdale from './pics/SILVERDALE.png';

function Home() {
    return (<>
        <div className='home-page'>
            <Carousel autoPlay={true} showThumbs={false} showIndicators={false} infiniteLoop={true} interval={7000} showStatus={false}>
                <img src={federalway} alt="Noun Project icon credits: olympic by sachan, cabin by Georgiana Ionescu, Sushi by suryanaagus, Dhalia by Pham Thanh Loc, crossword puzzle by Berkah Icon, paint by Iconbox, candy by Made" title="superimposed map of federal way scavenger hunt"></img>
                <img src={denver} alt="Noun Project icon credits: train clock by Made, secure by Adrien Coquet, checkers by Made" title="superimposed map of denver scavenger hunt"></img>
                <img src={silverdale} alt="Noun Project icon credits: Death Certificate by Jason D. Rowley, speed by sandiindra, Saxophone by Amethyst Studio, translate by Icon Lauk, reindeer by ArmOkay, National Flag and Ensign by Arthur Shlain, lightbulb by Maxim Kulikov" title="superimposed map of silverdale scavenger hunt"></img>
            </Carousel>
            <div className='about-us-container'><p>You've been training for this your whole life. Friends teased you for knowing so much about Taylor Swift, but now the only thing stopping a serial killer is your recollection of 'All Too Well' lyrics. Or your ability to tell a riesling from a chardonnay. Or your orienteering skills. Or whatever other talents you haven't showcased in a while.<br/><div className="indent">Welcome to Lovely Mysteries.</div></p>
            </div>
      </div>
      </>
    )
}

export default Home;