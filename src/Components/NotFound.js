import React from 'react';
import fourOhFourSymbol from './pics/404symbol.png';
import './NotFound.css';

function NotFound() {
    return (
        <div className="not-found-container">
            <img src={fourOhFourSymbol} className="four-symbol" alt="404 - page not found" title="recheck the url & try again" />
        </div>
    )
}

export default NotFound;