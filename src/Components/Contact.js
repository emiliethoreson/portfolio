import './Faq.css';
import React, { useState } from 'react';

function Contact(props) {

    const [successMessage, setSuccessMessage] = useState(false);
    const [failureMessage, setFailureMessage] = useState(false);

    const sendFaq = (event) => {
        event.preventDefault();
        props.emailjs.sendForm('contact_service', 'template_zraowhm', document.getElementById('faq-form'))
        .then(function() {
            setSuccessMessage(true);
        }, function(error) {
            console.log("FAILED::", error);
            setFailureMessage(true);
        })
    }

    const loadSuccessMessage = () => {
        const message = successMessage ? (<div className='success-faq'>Thank you! We'll be in touch soon.</div>) : (<></>)
        return message;
    }

    const loadFailureMessage = () => {
        const message = failureMessage ? (<div>Uh-oh, that didn't send. Please try again later.</div>) : (<></>)
        return message;   
    }

    return (
        <div className="contact-container">
            <h2>Contact Us</h2>
            <div className='faq-wrapper'>
                <h3>(253) 271-9066</h3>
                <h3><a id="contact-email" href="mailto:info@lovelymysteries.co">info@lovelymysteries.co</a></h3>
            </div>
            <div className='faq-wrapper'>
                <h3>Have a quick question for us? Big or small, send them our way & we'll respond within 24 hours.</h3>
                <form id="faq-form">
                    <label>Email:</label>
                    <input defaultValue="" type="email" name="faqEmail" className="faq-input faq-email"></input>
                    <label>Question:</label>
                    <textarea name="faqMessage" className="faq-input faq-question"></textarea>
                    <input className="submit-btn" type="submit" onClick={sendFaq} value="SEND"></input>
                    {loadSuccessMessage()}
                    {loadFailureMessage()}
                </form>
            </div>
        </div>
    )
}

export default Contact