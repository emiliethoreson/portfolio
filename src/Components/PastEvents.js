import React, { useState } from 'react';
import { events } from "./EventsObject.js";
import christmasPic from "./pics/christmas.jpg";
import lineupPic from "./pics/stone_family_lineup.jpg";
import carPic from "./pics/car.jpg";
import harryPic from "./pics/kidnapped.png";
import './PastEvents.css';

function PastEvents() {
    const images = [christmasPic, lineupPic, harryPic, carPic];
    const [active, setActive] = useState(0);

    const changeSlide = (pos) => {
      if (pos === 1 && active === events.length - 1) {
        setActive(0);
      } else if (pos === -1 && active === 0) {
        setActive(events.length - 1);
      } else {
        setActive(active + pos);
      }
    }

    return (
    <>
    <h2 className="past-events-title">Past Events Gallery</h2>
          <div className="scroll-button left-scroll" onClick={() => changeSlide(-1)}></div>
          <div className="scroll-button right-scroll" onClick={() => changeSlide(1)}></div>
          <div className="past-event-container">
            <h3>{events[active].title}</h3>
            <p className="event-summary">{events[active].plot}</p>
            <div className="results">
            <img src={images[active]} alt="summary pic" className="summary-pic" id={`events-pic-${active}`} />
            <p>{events[active].summary}</p>
            </div>
            <div className="review">
            <h4>Recipient review: "{events[active].quote}"</h4>
            </div>
        </div>
        </>
    )
};

export default PastEvents;
