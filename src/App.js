import './App.css';
import React, { useState } from 'react';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';
import Home from './Components/Home.js';
import Form from './Components/Form.js';
import PastEvents from './Components/PastEvents.js';
import AboutUs from './Components/AboutUs.js';
import Faq from './Components/Faq.js';
import Contact from './Components/Contact.js';
import NotFound from './Components/NotFound.js';
import logo from './Components/pics/logo.png';
import emailjs from 'emailjs-com';

function App() {
  let currentPath = window.location.pathname.substring(1);
  const [page, setPage] = useState((currentPath.length > 0 && currentPath !== 'portfolio/') ? currentPath : 'home');
  emailjs.init("user_KfxZ5VBu4aVxKMSM34eu4");

  return (
    <Router basename={process.env.PUBLIC_URL}>
      <nav className="nav-bar">
        <div className="point">
          <h1 className='name-space' onClick={() => setPage('home')}><Link to="/">Lovely Mysteries</Link></h1>
        </div>
        <div  className="point">
          <img src={logo} alt="Lovely Mysteries Logo" className="logo" title="Lovely Mysteries" />
        </div>
        <div className="top-nav-wrapper">
          <ul className="top-nav-ul">
            <li onClick={() => setPage('home')}><Link to="/">HOME</Link></li>
            <li onClick={() => setPage('book-now')}><Link to="/book-now">BOOK NOW</Link></li>
            <li onClick={() => setPage('gallery')}><Link to="/gallery">PAST EVENTS</Link></li>
          </ul>
        </div>
      </nav>
    <div className="App" id="scroll-box">
      <div className="content-container" id={page}>      
        <Switch>
          <Route path="/book-now">
            <Form emailjs={emailjs} />
          </Route>
          <Route path="/gallery">
            <PastEvents />
          </Route>
          <Route path="/faq">
            <Faq />
          </Route>
          <Route path="/about-us">
            <AboutUs />
          </Route>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/contact">
            <Contact emailjs={emailjs} />
          </Route>
          <Route>
            <NotFound />
          </Route>
        </Switch>
      </div>
    </div>
    <footer className='footer-specs'>
      <p>Lovely Mysteries is currently operating in Washington state.</p>
      <ul className="footer-nav-ul">
        <li onClick={() => setPage('faq')}><Link to="/faq">FAQ</Link></li>
        <li onClick={() => setPage('about-us')}><Link to="/about-us">Team</Link></li>
        <li onClick={() => setPage('contact')}><Link to="/contact">Contact</Link></li>
      </ul>
    </footer>
    </Router>
  );
}

export default App;
